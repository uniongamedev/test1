﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class Ball : MonoBehaviour {

    new Rigidbody2D rigidbody;
    float timer = 0;
    float maxTimer = 0;
    public int vector = 1;
    void Start () {
        if (!NetworkServer.active)
        {
            Destroy(this);
            return;
        }
        rigidbody = GetComponent<Rigidbody2D>();
        Destroy(gameObject, 10);
    }
   
    public Vector2 velocity;
	void Update () {
        velocity = rigidbody.velocity;
        rigidbody.velocity = new Vector2(velocity.x + vector * 2 * Time.deltaTime, velocity.y);
        
        if(Mathf.Abs(rigidbody.velocity.x) > 3)
        {
            ChangeVector();
        }

        timer += Time.deltaTime;
        if (timer >= maxTimer)
        {
            ChangeVector();
            maxTimer = Random.Range(0, 2f);
        }
    }
    void ChangeVector()
    {
        timer = 0;
        vector *= -1;
    }
    void OnCollisionEnter2D(Collision2D other)
    {
        switch (other.transform.tag)
        {
            case "Player":
                GameStatistics.AddPoints(1);
                BallSpawner.ballsArray.Remove(this);
                Destroy(this.gameObject);
                break;
            case "BallSpawner":
                GameStatistics.Restart();
                break;
            default:
                rigidbody.AddForce(Vector2.right * -other.relativeVelocity.x * 2, ForceMode2D.Impulse);
                break;
        }
        
    }
}
