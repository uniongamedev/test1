﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;
public class BallSpawner : MonoBehaviour {

    public Ball[] balls;
    public Vector3 boardPlace;
    public Transform boardPrefab;
    NetworkConnection networcConnection;
    public static List<Ball> ballsArray = new List<Ball>();
	// Use this for initialization
	public void StartGame () {
        NetworkServer.Listen(8080);

        GameStatistics.started = true;
        var obj = Instantiate(boardPrefab, boardPlace, Quaternion.identity) as Transform; 
        NetworkServer.Spawn(obj.gameObject);
	}
   
    float timer = 3;
    public static Vector2 force = new Vector2(0, -1.5f);
    public static float maxTimer = 3;
	void Update () {
        if (!GameStatistics.started)
            return;

        timer += Time.deltaTime;
        
        if(timer >= maxTimer)
        {
            var position = new Vector3(Random.Range(-6f, 6f), 5.5f, 0);

            var obj = Instantiate(balls[Random.Range(0, balls.Length - 1)], position , Quaternion.identity) as Ball;
            ballsArray.Add(obj);
            if(NetworkServer.active)
            {
                
                NetworkServer.Spawn(obj.gameObject);
            }

            obj.GetComponent<Rigidbody2D>().AddForce(force, ForceMode2D.Impulse);

            timer = 0;
            force *= 1.04f;
            maxTimer -= .01f;
        }
	}
}
