﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
public class NetClient : MonoBehaviour {
    NetworkClient nc;
    public Transform[] objToRegitration;
    public string IP = "127.0.0.1";
	// Use this for initialization
	public void Connect () {
        nc = new NetworkClient();
        nc.Connect(IP, 8080);
        nc.RegisterHandler(MsgType.Connect, Asd);
        nc.RegisterHandler(1001, SetScore);
    }
	
	// Update is called once per frame
	public void SetIP (string ip) {
        IP = ip;
    }
    void Asd(NetworkMessage netMsg)
    {
        ClientScene.Ready(nc.connection);
        ClientScene.AddPlayer(nc.connection, 0);
        foreach(Transform obj in objToRegitration)
        {
            ClientScene.RegisterPrefab(obj.gameObject);
        }
        Debug.Log("Connect");
    }
    void SetScore(NetworkMessage netMsg)
    {
       GameStatistics.SetPoints(netMsg.ReadMessage<StringMessage>().value);
    }
}
