﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using System.Collections;
using System.Collections.Generic;

public class GameStatistics : MonoBehaviour
{

    public static int points;
    public static Text pointsDisplay;
    public static bool started = false;
    // Use this for initialization
    void Start()
    {
        pointsDisplay = transform.GetChild(1).GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            started = !started;
            if (Time.timeScale > 0)
                Time.timeScale = 0;
            else
                Time.timeScale = 1;
        }
    }
    public static void AddPoints(int p)
    {
        points += p;
        pointsDisplay.text = points.ToString();
        if (!NetworkServer.active)
            return;
        var msg = new StringMessage(points.ToString());
        NetworkServer.SendToAll(1001, msg);
    }
    public static void SetPoints(string s)
    {
        pointsDisplay.text = s;
    }
    public static void Quit()
    {
        Application.Quit();
    }
    public static void Restart()
    {
        pointsDisplay.text = "0";
        foreach (Ball ball in BallSpawner.ballsArray)
        {
            Destroy(ball.gameObject);
        }
        BallSpawner.force = new Vector2(0, -1.5f);
        BallSpawner.maxTimer = 3;
    }
}
